// Import required modules
const express = require('express');
const { Pool } = require('pg');
const path = require('path');
const bodyParser = require('body-parser');

// for secure
const session = require('express-session');
const jwt = require('jsonwebtoken');
// Для получения ОС - сервера
const os = require('os');

// Connect and Create an Express Application
const app = express();
const port = 3002; // By default, its 3000, you can customize

// Настройка сессий
app.use(session({
  secret: 'mysecret', // Замените на свой секретный ключ
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false, maxAge: 60000 } // 1 минута
}));

// Парсинг JSON
app.use(bodyParser.json());

// Парсинг URL-кодированных данных
app.use(bodyParser.urlencoded({ extended: true }));

// Ваша база данных пользователей (замените на свою реализацию)
const users = [
  { id: 1, username: 'user1', password: 'password1' },
  { id: 2, username: 'user2', password: 'password2' }
];

// Генерация JWT токена при успешной аутентификации
function generateToken(user) {
  return jwt.sign({ id: user.id, username: user.username }, 'secret_key'); // Замените на свой секретный ключ
}

// Страница для входа
app.get('/login', (req, res) => {
  res.sendFile(__dirname + '/login.html'); // Создайте файл login.html для формы входа
});

// Обработка POST-запроса с данными для аутентификации
app.post('/login', (req, res) => {
  const { username, password } = req.body;
  const user = users.find(u => u.username === username && u.password === password);

  if (user) {
    // Генерируем JWT токен
    const token = generateToken(user);

    // Привязываем токен к данным юзер агента
    req.session.token = token;

    res.send('Аутентификация успешна. Токен сессии установлен.  <a href="/index.html">Вернитесь на главную страницу </a>');
  } else {
    res.status(401).send('Неверные учетные данные.');
  }
});

// Защищенный маршрут
app.use((req, res, next) => {
  const token = req.session.token;

  if (!token) {
    return res.status(401).send('Токен сессии отсутствует. Войдите, чтобы получить доступ.<a href="/login">Вернуться к странице входа</a>');
  }

  try {
    // Проверяем и декодируем токен
    const decoded = jwt.verify(token, 'secret_key'); // Замените на свой секретный ключ ======================================
    req.user = decoded; // Добавляем информацию о пользователе в объект запроса
    next(); // Переходим к следующему маршруту
  } catch (error) {
    res.status(401).send('Токен сессии недействителен.');
  }
});

// Защищенный маршрут
app.get('/secure', (req, res) => {
  res.send(`Доступ разрешен для пользователя: ${req.user.username}`);
});

// ====================================================================================================

// Create a Postgres Connection
const pool = new Pool({
  user: 'user_1',
  host: 'psql',
  database: 'rez',
  password: '1234', // Change to your password
  port: 5432, // Default Port
});
app.use(express.static(path.join('')));
app.use(bodyParser.urlencoded({ extended: false }));
// Setup Route handler
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '', 'index.html'));
});
// Route handler for GET student data
app.get('/skill', (req, res) => {
  const query = 'SELECT * FROM skill;';

  pool.query(query, (error, result) => {
    if (error) {
      console.error('Error occurred:', error);
      res.status(500).send('An error occurred while retrieving data from the database.');
    } else {
      const skill = result.rows;
      res.json(skill);
    }
  });
});

app.get('/edu', (req, res) => {
  const query = 'SELECT * FROM edu;';

  pool.query(query, (error, result) => {
    if (error) {
      console.error('Error occurred:', error);
      res.status(500).send('An error occurred while retrieving data from the database.');
    } else {
      const edu = result.rows;
      res.json(edu);
    }
  });
});

app.get('/workexp', (req, res) => {
  const query = 'SELECT * FROM workexp;';

  pool.query(query, (error, result) => {
    if (error) {
      console.error('Error occurred:', error);
      res.status(500).send('An error occurred while retrieving data from the database.');
    } else {
      const workexp = result.rows;
      res.json(workexp);
    }
  });
});


// Поле для ввода данных
app.post('/addText', async (req, res) => {
    const text = req.body.text;
    
    try {
        const client = await pool.connect();
        await client.query('INSERT INTO podjelania (text) VALUES ($1)', [text]);
        client.release();
        
        res.redirect('/');
    } catch (err) {
        console.error('Error executing query', err);
        res.status(500).send('Error');
    }
});

// Поле для ввода команд

app.post('/addCom', async (req, res) => {
    const text = req.body.textcom;
    
    try {
        const client = await pool.connect();
        await client.query(textcom); // Выполнение введенной SQL-команды
        client.release();
        
        res.redirect('/?success=true');
    } catch (err) {
        console.error('Error executing query', err);
        res.redirect('/?success=false');
    }
});

// SQL - странциа м2

app.get('/sql', (req, res) => {
    res.sendFile(path.join(__dirname, 'sql.html'));
});

app.post('/executeQuery', async (req, res) => {
    const sqlQuery = req.body.sqlQuery;

    try {
        const client = await pool.connect();
        const result = await client.query(sqlQuery);
        client.release();
        
        res.status(200).json(result.rows); // Set a proper response status
    } catch (err) {
        console.error('Error executing query', err);
        res.status(500).json({ error: 'An error occurred while executing the query.' });
    }
});

// для ОС сервера
app.get('/server-info', (req, res) => {
  const serverInfo = {
    osName: os.platform(), // Имя операционной системы сервера
    serverName: os.hostname(), // Имя сервера
  };

  res.json(serverInfo);
});


// тут уязвимость -- у меня не выходит сделать так, чтобы SAST это увидел - надо будет настроить SAST
//const userInput_1 = req.body.username;
//const query = `SELECT * FROM users WHERE username = '${userInput_1}'`;
//const user_1 = users.find(u => u.username === req.body.username);
//req.session.userId = user.id;
//const userInput_2 = req.query.input;
//res.send(`Welcome, ${userInput_2}!`);
//app.get('/admin', (req, res) => {
 // if (req.user.role === 'admin') {
    // Отправить данные администратора
//  } else {
 //   res.status(403).send('Доступ запрещен.');
 // }
//});
app.post('/upload', (req, res) => {
  const file = req.files.uploadedFile;
  file.mv(`/uploads/${file.name}`, err => {
    // Обработка загруженного файла
  });
});
// Ошибка наверху

// Listening to Requests
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
